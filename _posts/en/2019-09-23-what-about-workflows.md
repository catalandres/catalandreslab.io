---
title: "...and what about workflows?"
lang: en
ref: workflows
categories: [Salesforce]
tags: [Process Builder, workflows]
excerpt: "At a certain point someone innocently raised her hand and asked: \"Why bother trying to manage all this Process Builder complexity when we can just get it done with a workflow?\" Challenge accepted."
---

Last July I participated in the [Barcelona Administrators Group](https://trailblazercommunitygroups.com/barcelona-es-administrators-group/).
While I had prepared a simplified version of a piece on how to use custom notifications that I had presented for the Philadelphia Developers Group, the highlight of the day came at the time for the whole group to discuss best practices for Salesforce administrators.

At a certain point I was chiming in with a few ideas on how we can make Process Builder more reliable, and someone innocently raised her hand and asked:

> **Why bother trying to manage all this Process Builder complexity when we can just get it done with a workflow?**

Challenge accepted.
This is an expanded version of my remarks on that day.

## Build a reliable system

A small detail that is a bigger deal than you may think: **there is no way to determine in what order two workflow rules on the same object will be executed**.
In other words: when we define multiple workflow rules on the same object, we cannot predict the outcome of an automation run.
This defeats the purpose of letting a system figure out how it should respond to our actions.

Processes let you determine in which order the system should check conditions and run actions, and, for those cases that are more complex, we can call subprocesses or even flows to take care of that logic.
And if (or when) something goes wrong, processes are easier to debug, as you can configure your org to send you very detailed emails every time there is an error.
This means less time to fix the issue, letting your users go back to business faster.

## Future-proof your automations

Face the reality: at this point workflow rules are in maintenance mode as a Salesforce feature.
The last two significant additions to workflows were part of the Spring'18 release ([groups and topics](https://releasenotes.docs.salesforce.com/en-us/spring18/release-notes/rn_networks_process_more_objects.htm), and [Chatter](https://releasenotes.docs.salesforce.com/en-us/spring18/release-notes/rn_collab_groups_process_builder.htm)), and they were released for both workflow rules and Process Builder.
It is unlikely that we will see a transformative release that improves workflow rules significantly beyond what is already available.

On the other hand, **Process Builder can do virtually everything that workflow rules do**.
And it does so without facing the specific limits of workflows: while there can only be 50 active workflow rules per object, with Process Builder you can build as many decision nodes in a process as you deem necessary.
Processes are, however, bound to the same limits and rules as (synchronous) Apex and flows: the [governor limits](https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/apex_gov_limits.htm), which determine how many times you can read from the database (SOQL queries) and write to it (DML operations), as well as how much time your calculations should take (CPU time).

Despite feature parity and all, there are a couple of feature gaps worthwhile mentioning:

* [**Outbound Messages.**](https://help.salesforce.com/articleView?id=workflow_managing_outbound_messages.htm&type=5) Sending a call to an external service is simpler in workflow rules, as long as the requirements match the effective-but-narrow capabilities of outbound messages. For instance, if you need to authenticate to send the message the solution is not straightforward and you cannot use named credentials. For that degree of versatility, one would have to [resort to Apex](https://salesforce.stackexchange.com/questions/225317/how-to-change-record-type-using-salesforce-process-builder-without-using-record).
* **Record Types.** Believe it or not, workflow rules are still the simplest and easiest way to change the record type of a record based on the value of its fields. Also, it is the friendliest from a release management perspective, because any other solution based on Process Builder that does not [involve Apex](https://salesforce.stackexchange.com/questions/225317/how-to-change-record-type-using-salesforce-process-builder-without-using-record) or a flow will probably have to rely on hardcoded record type IDs, which are environment specific and cannot be migrated safely. I would not be surprised if in the near future we see advances in Salesforce's roadmap that address this shortcoming, especially if they involve [custom metadata types](https://help.salesforce.com/articleView?id=custommetadatatypes_about.htm&type=5).

## Maximize your return on investment

Through its subscription pricing, we pay Salesforce to deliver a steady flow of innovation that improves the product in every release.
Sorry for appealing to your [fomo](http://fomo.urbanup.com/7159436), but if you keep using workflow rules instead of Process Builder you will be missing out big time from all that innovation.

For instance, let's take a look at what actions you can use in a process as of Winter'20:

* You can call an Apex class.
* You can create a record of almost any kind.
* You can send email alerts.
* You can call a flow.
* You can post to Chatter.
* You can call another process.
* You can execute a quick action.
* You can perform all sorts of wizardry and witchcraft in or around Quip documents.
* You can send custom notifications.
* You can submit records automatically for approval.
* You can update any records that are related to the record that initiated the process.

Note that about a fourth of all this functionality was added in the past 3--4 releases, including the ability to run processes when the system receives a platform event.

If you are on the fence, waiting to adopt Process Builder, here is the great news: transitioning your workflows to processes is extremely easy.
To assist you in your journey, you can tap a brief and insightful Trailhead module on [how to migrate workflow rules](https://trailhead.salesforce.com/en/content/learn/modules/workflow_migration), as well as a very comprehensive [list of best practices](https://help.salesforce.com/articleView?id=process_considerations_design_bestpractices.htm&type=5) in the Salesforce documentation.

Jump into Process Builder --- make your automations more reliable and scalable, and put them in the best position to use all the new features!
