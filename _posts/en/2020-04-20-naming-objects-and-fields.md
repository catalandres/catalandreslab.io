---
title: "Naming objects and fields"
lang: en
ref: workflows
categories: [Salesforce]
tags: [Best Practices, The Elements of Salesforce]
excerpt: "Here are a few ideas that we can use when naming objects and fields."
---

This is the second article in "The elements of Salesforce", a running series of posts named after [Strunk and White's style guide](https://en.wikipedia.org/wiki/The_Elements_of_Style). In these articles, I try to describe the practices that I find myself converging towards. I update them as appropriate. Feel free to send over any feedback or ideas.

Almost every metadata component in Salesforce has two names, the external, user-facing one, also called "label", and the internal one, to be used by code and APIs, which is also called "API name" or, at times, just "name". 

In this article we will take a look at a few naming practices that we can use when dealing with objects and fields, so that development becomes more sustainable.

## Objects

The label and API name should:

* Contain only [common nouns](https://en.wikipedia.org/wiki/Proper_noun)
* Be singular
* Be as [parsimonious](https://en.wikipedia.org/wiki/Parsimony) as common sense would dictate

Any deviation from the above should be inspected as an early sign of deeper flaws in the underlying data model.

Note that, in the case of junction objects (objects with master-detail relationships to other two objects), it is common to append the names of both parent objects if there is no viable term to use in the business domain.

## Field sufixes and formulas

It is relatively common to end up with multiple fields with the same name. In that case, use a suffix to differentiate them.

Here are a few suffixes we can use, but treat them as suggestions:

* Formula fields, in general, can use `_f` or `_formula` as a suffix.
* [Indicator functions](https://en.wikipedia.org/wiki/Indicator_function) (numeric Booleans) can use `_i` or `_indicator`.
* Formula fields created for display purposes (with images, colors, icons, etc.) can use `_d` or `_display`.

## Date and time fields

If a field can be easily identified as a date, a time or a timestamp (date/time) by reading its label, then use the label in the API name. If not, make sure that the last word in the API name is:

* `Date` for date fields
* `Time` for time fields
* `Moment`, `Timestamp` or `DateTime` for date/time fields

For example, `HebrewBirthday__c` and `SomethingHappenedDate__c` are both appropriate date field names.

All of the above applies to date, time and date/time formulas.

## Checkboxes

The label of a checkbox (or Boolean) field should be one of the following:

* A noun, or a nominal [syntagma](https://en.wikipedia.org/wiki/Syntagma_(linguistics)) --- that is, a noun and its complements: articles, determinants, adjectives
* An adjective, or an adjectival syntagma
* A verb in [infinitive](https://en.wikipedia.org/wiki/Infinitive), [gerund](https://en.wikipedia.org/wiki/Gerund), or [participle](https://en.wikipedia.org/wiki/Participle), or a [modal verb](https://en.wikipedia.org/wiki/Modal_verb), all of which would be preferred over a conjugated verb in personal form. The verb should be omitted whenever reasonably possible.

The label should neither be structured as a question nor include a question mark, as a question mark is an invitation to answer a yes/no question, and such questions are often not binary, but ternary, because we are also interested in determining whether the user has responded the question or not. Whenever answering a question negatively is not equivalent to not answering the question, use a picklist with "Yes", "No" and null as the options.

The API name of a checkbox, however, should be structured as a question, with `Is`, `Has`, `Does`, or a modal verb as its first word.

Label | API name
--- | ---
Active | `IsActive__c`
Emotional Support Animal | `IsEmotionalSupportAnimal__c`
From Another Shelter | `IsFromAnotherShelter__c`
Must Wash Hands | `MustWashHands__c`
Playing in Primera División | `IsPlayingInPrimeraDivision__c`<br/>`DoesPlayInPrimeraDivision__c`
Traveled Abroad | `HasTraveledAbroad__c`
European Union Passport | `HasEuropeanUnionPassport__c`
Tip Included | `DoesIncludeTip__c`<br/>`IsTipIncluded__c`

All the above also applies to checkbox formulas.

### Revision history

2020-04-20 --- Published
