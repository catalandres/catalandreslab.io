---
title: "Professor Lehman and the cloud"
lang: en
ref: workflows
categories: [Salesforce]
tags: [Today I learned, Software development, DevOps]
excerpt: "While preparing for a Dreamforce talk on DevOps, I stumbled upon Lehman's laws of software evolution.
I find them fascinating. What drives the decay of software? What can we do to avoid it?"
---

While preparing for a Dreamforce talk on DevOps, I stumbled upon Lehman's laws of software evolution.

Throughout an academic career spanning three decades, [Professor Meir Lehman](https://en.wikipedia.org/wiki/Manny_Lehman_(computer_scientist)) studied the evolution of software, and formulated eight evolution laws describing how the systems that solve real-world problems work.
Note that these are empirical laws, which means that they are considered to be right until proved otherwise.

The following is a direct lift and slight adaptation of the [Wikipedia article](https://en.wikipedia.org/wiki/Lehman%27s_laws_of_software_evolution):

<!-- markdownlint-disable no-inline-html -->
> 1. **Continuing change.** <mark>A system must be continually adapted or it becomes progressively less satisfactory.</mark>
> 2. **Increasing complexity.** As a system evolves, <mark>its complexity increases unless work is done to maintain or reduce it.</mark>
> 3. **Self regulation.** System evolution processes are self-regulating with the distribution of product and process measures close to normal.
> 4. **Conservation of organizational stability.** The average effective global activity rate in an evolving system is invariant over the product's lifetime.
> 5. **Conservation of familiarity.** As a system evolves, <mark>all stakeholders</mark> associated with it, developers, sales personnel and users, for example, <mark>must maintain mastery of its content and behavior to achieve satisfactory evolution. Excessive growth diminishes that mastery.</mark> Hence the average incremental growth remains invariant as the system evolves.
> 6. **Continuing growth.** <mark>The functional content of a system must be continually increased to maintain user satisfaction over its lifetime.</mark>
> 7. **Declining quality.** <mark>The quality of a system will appear to be declining unless it is rigorously maintained and adapted to operational environment changes.</mark>
> 8. **Feedback system.** System evolution processes constitute multi-level, multi-loop, multi-agent feedback systems and must be treated as such to achieve significant improvement over any reasonable base.
<!-- markdownlint-restore -->

Basically, these ideas have been around for over half a century. Lehman's first work was published in 1974, 45 years ago, after he and [László Bélády](https://en.wikipedia.org/wiki/L%C3%A1szl%C3%B3_B%C3%A9l%C3%A1dy) worked on it together at IBM.
They applied to mainframe software, perforated cards, and magnetic discs.
They applied to microcomputers and token networks.
And they apply in the era of cloud computing, open source, and software as a service.

These laws identify the forces that drive the decay of software if it is left to its own inertia, and it serves as a list of some or all the efforts required to keep it up to par with the real-world needs that it has to satisfy.
I believe that most of the techniques included in what we currently call DevOps are an answer to these challenges.
