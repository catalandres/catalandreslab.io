In traditional enterprise software models, companies would buy the software once with a large lump sum payment and then pay the software company a recurring payment for maintenance and support.
The features would remain stable (or, some would say, fossilized) until the software company would convince their customer to make another large lump sum payment for an upgrade to the next version.

Modern subscription-based enterprise software works differently, because there is only one recurring payment that covers:

* Maintenance and support
